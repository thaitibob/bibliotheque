### Projet Bibliothèque

Code pour réaliser le projet bibliothèque.


### Configuration nécessaire pour exécuter l'application

Nous donnons les commandes pour Linux, mais vous pouvez évidement trouver facilement les équivalents sur internet pour MAC et WINDOWS.

Installation de docker
```commande
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker
```
Installation de docker-compose
```commande
sudo apt install docker-compose
```


## Récupération du projet
Copier le lien pour le clone à partir du bouton "clone" de la page bitbucket du projet (en haut à droite.  
Le lien sera de la forme 
```commande
git clone https://Brice543@bitbucket.org/thaitibob/bibliotheque.git
```

Se placer dans l'emplacement voulu à partir du terminal et coller le lien. La commande va alors créer un dossier avec l'ensemble des éléments du projet. 


### Déploiement de l'application

Se positionner à la racine du projet pour lancer le docker-compose (faire attention de bien avoir Docker de lancé) 
```commande
sudo docker-compose up --build -d
```

Si jamais un port est déjà en utilisation, il est possible d'arrêter l'application sur ce port avec les commandes suivantes: 
```commande
sudo lsof -i:PortApplication
kill -15 <PID_Application>
```
A noter: Si jamais le port déjà utilisé est celui pour la base MySQL, cela signifie q'uil y a déjà une instance MySQL de lancée. 
Il faudra alors l'arrêter à la main car la commande ne sera pas autorisée. 


### Vérification

Pour vérifier le bon fonctionnement de l'api-bilbliotheque
```commande
http://localhost:9090/user
```
Pour vérifier le bon fonctionnement de l'interface (clientui)
```commande
http://localhost:8080/
```
Pour vérifier le bon fonctionnement de la BDD (phpmyadmin)
```commande
#Utilisateur    : root
#Mot de passe   : admin
http://localhost:8081/
```


### Stopper l'application

Pour stopper l'applciation, il suffit de, toujours à la racine du projet, taper la commande suivante dans le terminal: 
```commande
docker-compose stop
```



