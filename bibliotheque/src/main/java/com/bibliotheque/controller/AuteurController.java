package com.bibliotheque.controller;

import com.bibliotheque.entity.Auteur;
import com.bibliotheque.entity.Livre;
import com.bibliotheque.entity.Oeuvre;
import com.bibliotheque.entity.User;
import com.bibliotheque.repository.AuteurRepository;
import com.bibliotheque.repository.OeuvreRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/auteur", produces= MediaType.APPLICATION_JSON_VALUE)
public class AuteurController {

    private final AuteurRepository auteurRepository;
    private final OeuvreRepository oeuvreRepository;

    public AuteurController(AuteurRepository auteurRepository, OeuvreRepository oeuvreRepository) {
        this.auteurRepository = auteurRepository;
        this.oeuvreRepository = oeuvreRepository;
    }

    @GetMapping
    public ResponseEntity<?> getAllAuteur() {
        Iterable<Auteur> all = auteurRepository.findAllByDelete(false);
        return ResponseEntity.ok(all);
    }

    @GetMapping(path="/{auteurId}")
    public ResponseEntity<?> getAuteur(@PathVariable("auteurId") int id) {
        Optional<Auteur> auteur = auteurRepository.findById(id);
        return auteur.isEmpty()
                ? ResponseEntity.notFound().build()
                : ResponseEntity.ok(auteur);
    }

    @GetMapping(path="/{auteurId}/livre")
    public ResponseEntity<?> getLivreAuteur(@PathVariable("auteurId") int id) {
        List<Oeuvre> oeuvres = oeuvreRepository.FindLivrePourAuteur(id);
        return ResponseEntity.ok(oeuvres);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> addAuteur(@RequestBody Auteur auteur) {
        Auteur i = new Auteur(auteur.getNom(), auteur.getPrenom());
        Auteur save = auteurRepository.save(i);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @GetMapping(path="/livre/{livreId}")
    public ResponseEntity<?> getAuteurParLivre(@PathVariable("livreId") int livreId) {
        List<Auteur> auteur = auteurRepository.FindAuteursPourLivre(livreId);
        return ResponseEntity.ok(auteur);
    }

    @PostMapping(value = "/{auteurId}/livre/{livreId}")
    @Transactional
    public ResponseEntity<?> addAuteurLivre(@PathVariable("auteurId") int auteurId, @PathVariable("livreId") int livreId) {
        Optional<Auteur> a = auteurRepository.findById(auteurId);
        Optional<Oeuvre> e = oeuvreRepository.findById(livreId);
        if(a.isPresent() && e.isPresent()){
            Auteur auteur = a.get();
            Livre livre = (Livre) e.get();
            livre.addAuteur(auteur);
            Oeuvre save = oeuvreRepository.save(livre);
            return new ResponseEntity<>(save, HttpStatus.CREATED);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping(value = "/{auteurId}")
    @Transactional
    public ResponseEntity<?> updateAuteur(@RequestBody Auteur auteur, @PathVariable("auteurId") int auteurId) {
        Optional<Auteur> body = Optional.ofNullable(auteur);
        if (body.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        if (!auteurRepository.existsById(auteurId)) {
            return ResponseEntity.notFound().build();
        }
        auteur.setId(auteurId);
        Auteur result = auteurRepository.save(auteur);
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{auteurId}")
    @Transactional
    public ResponseEntity<?> deleteAuteur(@PathVariable("auteurId") int auteurId) {
        Optional<Auteur> auteur = auteurRepository.findById(auteurId);
        if (auteur.isPresent()) {
            auteur.get().setDelete(true);
            auteurRepository.save(auteur.get());
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }
}
