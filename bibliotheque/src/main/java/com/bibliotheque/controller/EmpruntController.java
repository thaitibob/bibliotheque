package com.bibliotheque.controller;

import com.bibliotheque.entity.*;
import com.bibliotheque.repository.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import com.bibliotheque.repository.EmpruntRepository;
import com.bibliotheque.repository.OeuvreRepository;
import com.bibliotheque.repository.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path="/emprunt", produces= MediaType.APPLICATION_JSON_VALUE)
public class EmpruntController {

    private final UserRepository userRepository;
    private final ExemplaireRepository exemplaireRepository;
    private final OeuvreRepository oeuvreRepository;
    private final EmpruntRepository empruntRepository;

    public EmpruntController(UserRepository userRepository, OeuvreRepository oeuvreDao, ExemplaireRepository exemplaireDao, EmpruntRepository empruntDao) {
        this.userRepository = userRepository;
        this.exemplaireRepository = exemplaireDao;
        this.oeuvreRepository = oeuvreDao;
        this.empruntRepository = empruntDao;
    }

    @GetMapping
    public ResponseEntity<?> getAllEmprunts(@RequestParam(value = "oeuvreId") int oeuvreId) {
        List<Emprunt> all = empruntRepository.EmpruntByOeuvre(oeuvreId);
        List<EmpruntOutPut> outputs = this.listEmprunts(all);
        return ResponseEntity.ok(outputs);
    }

    @GetMapping(path="/user/{userId}/{empruntId}")
    public ResponseEntity<?> getEmpruntByIdAndUser(@PathVariable("userId") int id, @PathVariable("empruntId") int empruntId) {
        Optional<Emprunt> emprunt1 = empruntRepository.findById(empruntId);
        EmpruntOutPut o1 = new EmpruntOutPut();
        if(emprunt1.isPresent()){
            Emprunt emprunt = emprunt1.get();
            o1 = setEmpruntOutput(emprunt, o1);
            return ResponseEntity.ok(o1);
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping(path="/user/{userId}")
    public ResponseEntity<?> getEmpruntUser(@PathVariable("userId") int id) {
        List<Emprunt> all = empruntRepository.findByUserId(id);
        List<EmpruntOutPut> outputs = this.listEmprunts(all);
        return ResponseEntity.ok(outputs);
    }

    @PostMapping(path="/user/{userId}/oeuvre/{oeuvreId}/exemplaire/{exemplaireId}")
    @Transactional
    public ResponseEntity<?> addEmprunt(@PathVariable("userId") int userId, @PathVariable("oeuvreId") int oeuvreId, @PathVariable("exemplaireId") int exemplaireId, @RequestBody Emprunt emprunt) {
        Optional<User> user = userRepository.findById(userId);
        Optional<Exemplaire> exemplaire = oeuvreRepository.FindExemplaireIdPourOeuvreStatut(oeuvreId,exemplaireId, "Disponible");

        if(user.isPresent() && exemplaire.isPresent()){
            // ajout de l'emprunt
            Emprunt e = new Emprunt(user.get(), exemplaire.get(), emprunt.getDureeEmprunt());
            Emprunt save = empruntRepository.save(e);

            // on passe l'exemplaire a emprunté
            exemplaire.get().setEtat("Emprunté");
            exemplaireRepository.save(exemplaire.get());
            return new ResponseEntity<>(save, HttpStatus.CREATED);
        }
        return ResponseEntity.notFound().build();
    }

    @PatchMapping(path = "/user/{userId}/oeuvre/{oeuvreId}/exemplaire/{exemplaireId}/{empruntId}", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateEmprunt(@PathVariable("userId") int userId,
                                           @PathVariable("oeuvreId") int oeuvreId,
                                           @PathVariable("exemplaireId") int exemplaireId,
                                           @PathVariable("empruntId") int empruntId,
                                           @RequestBody EmpruntOutPut emprunt) {
        Optional<Emprunt> emprunt1 = empruntRepository.findById(empruntId);
        if(emprunt1.isPresent()){
            Emprunt e = emprunt1.get();

            //Ajout du nombre de jours de retard lors du rendu
            String str = e.getDateEmprunt();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime date_emprunt = LocalDateTime.parse(str, formatter);
            LocalDateTime date_rendu = LocalDateTime.now();
            long diff = ChronoUnit.DAYS.between(date_emprunt, date_rendu);
            String retard1 = "Pas de retard";
            if(diff>e.getDureeEmprunt()){
                diff = diff-e.getDureeEmprunt();
                String retard = Long.toString(diff);
                retard1 = retard+" jour(s)";
            }

            e.setRetard(retard1);
            e.setDateRendu(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
            e.setCommentaire(emprunt.getCommentaire());
            Emprunt save = empruntRepository.save(e);
            return new ResponseEntity<>(save, HttpStatus.OK);
        }
        return ResponseEntity.notFound().build();
    }

    private List listEmprunts(List<Emprunt> all){
        List<EmpruntOutPut> outputs = new ArrayList<>();
        all.forEach(emprunt -> {
            EmpruntOutPut o1 = new EmpruntOutPut();
            o1 = setEmpruntOutput(emprunt, o1);
            outputs.add(o1);
        });
        return outputs;
    }

    private EmpruntOutPut setEmpruntOutput(Emprunt emprunt, EmpruntOutPut o1){
        o1.setId(emprunt.getId());
        o1.setDateEmprunt(emprunt.getDateEmprunt());
        o1.setDateRendu(emprunt.getDateRendu());
        o1.setDureeEmprunt(emprunt.getDureeEmprunt());
        o1.setRetard(emprunt.getRetard());
        o1.setId_user(emprunt.getUser().getId());
        o1.setNom(emprunt.getUser().getNom());
        o1.setPrenom(emprunt.getUser().getPrenom());
        o1.setId_oeuvre(emprunt.getExemplaire().getOeuvre().getId());
        o1.setTitre(emprunt.getExemplaire().getOeuvre().getTitre());
        o1.setType_oeuvre(emprunt.getExemplaire().getOeuvre().getType_oeuvre());
        o1.setId_exemplaire(emprunt.getExemplaire().getId());
        o1.setCommentaire(emprunt.getCommentaire());
        o1.setEtat(emprunt.getExemplaire().getEtat());
        return o1;
    }

}
