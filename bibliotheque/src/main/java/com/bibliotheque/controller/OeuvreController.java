package com.bibliotheque.controller;

import com.bibliotheque.entity.*;
import com.bibliotheque.repository.ExemplaireRepository;
import com.bibliotheque.repository.LivreRepository;
import com.bibliotheque.repository.MagazineRepository;
import com.bibliotheque.repository.OeuvreRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path="/oeuvre", produces= MediaType.APPLICATION_JSON_VALUE)
public class OeuvreController {

    private final MagazineRepository magazineRepository;
    private final OeuvreRepository oeuvreRepository;
    private final LivreRepository livreRepository;
    private final ExemplaireRepository exemplaireRepository;

    public OeuvreController(MagazineRepository magazineRepository, OeuvreRepository oeuvreDao, LivreRepository livreDao, ExemplaireRepository exemplaireDao) {
        this.magazineRepository = magazineRepository;
        this.oeuvreRepository = oeuvreDao;
        this.livreRepository = livreDao;
        this.exemplaireRepository = exemplaireDao;
    }

    @GetMapping
    public ResponseEntity<?> getAllOeuvres() {
        Iterable<Oeuvre> all = oeuvreRepository.findAllByDelete(false);
        return ResponseEntity.ok(all);
    }

    @GetMapping(path="/{oeuvreId}")
    public ResponseEntity<?> getOeuvre(@PathVariable("oeuvreId") int id) {
        Optional<Oeuvre> oeuvre = oeuvreRepository.findById(id);
        return oeuvre.isEmpty()
                ? ResponseEntity.notFound().build()
                : ResponseEntity.ok(oeuvre);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> addOeuvre(@RequestBody Oeuvre oeuvre) {
        switch(oeuvre.getType_oeuvre()) {
            case "livre":
                Livre livre = (Livre) oeuvre;
                Livre l = new Livre(livre.getTitre(), livre.getDateParution(), livre.getEdition(), livre.getResume(), oeuvre.getType_oeuvre(), ((Livre) oeuvre).getAuteurs());
                Livre saveL = livreRepository.save(l);
                return new ResponseEntity<>(saveL, HttpStatus.CREATED);
            case "magazine":
                Magazine magazine = (Magazine) oeuvre;
                Magazine m = new Magazine(magazine.getTitre(),magazine.getDateParution(),magazine.getNumero(),magazine.getPeriodicite(), magazine.getType_oeuvre());
                Magazine saveM = magazineRepository.save(m);
                return new ResponseEntity<>(saveM, HttpStatus.CREATED);
            default:
                return ResponseEntity.badRequest().build();
        }
    }

    @PatchMapping(value = "/{oeuvreId}")
    @Transactional
    public ResponseEntity<?> updateFieldOeuvre(@RequestBody Oeuvre oeuvre, @PathVariable("oeuvreId") int Id) {
        Optional<Oeuvre> oeuvre1 = oeuvreRepository.findById(Id);
        if(oeuvre1.isPresent()){
            Oeuvre i = oeuvre1.get();
            if(oeuvre.getTitre() != null){
                i.setTitre(oeuvre.getTitre());
            }
            if(oeuvre.getDateParution() != null){
                i.setDateParution(oeuvre.getDateParution());
            }
            switch(oeuvre.getType_oeuvre()) {
                case "livre":
                    Livre livre = (Livre) i;
                    Livre livre1 = (Livre) oeuvre;
                    if(livre1.getEdition() != null){
                        livre.setEdition(livre1.getEdition());
                    }
                    if(livre1.getResume() != null){
                        livre.setResume(livre1.getResume());
                    }
                    Livre saveL = livreRepository.save(livre);
                    return new ResponseEntity<>(saveL, HttpStatus.OK);
                case "magazine":
                    Magazine magazine = (Magazine) i;
                    Magazine magazine1 = (Magazine) oeuvre;
                    if(magazine1.getNumero() != 0){
                        magazine.setNumero(magazine1.getNumero());
                    }
                    if(magazine1.getPeriodicite() != null){
                        magazine.setPeriodicite(magazine1.getPeriodicite());
                    }
                    Magazine saveM = magazineRepository.save(magazine);
                    return new ResponseEntity<>(saveM, HttpStatus.OK);
            }
            Oeuvre save = oeuvreRepository.save(i);
            return new ResponseEntity<>(save, HttpStatus.OK);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping(value = "/{oeuvreId}")
    @Transactional
    public ResponseEntity<?> updatemagazine(@RequestBody Oeuvre oeuvre, @PathVariable("oeuvreId") int Id) {
        switch(oeuvre.getType_oeuvre()) {
            case "livre":
                Livre livre = (Livre) oeuvre;
                if (!livreRepository.existsById(Id))   return ResponseEntity.notFound().build();
                livre.setId(Id);
                Livre saveL = livreRepository.save(livre);
                return new ResponseEntity<>(saveL, HttpStatus.OK);
            case "magazine":
                Magazine magazine = (Magazine) oeuvre;
                if (!magazineRepository.existsById(Id))   return ResponseEntity.notFound().build();
                magazine.setId(Id);
                Magazine saveM = magazineRepository.save(magazine);
                return new ResponseEntity<>(saveM, HttpStatus.OK);
            default:
                return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping(value = "/{oeuvreId}")
    @Transactional
    public ResponseEntity<?> deleteOeuvre(@PathVariable("oeuvreId") int id) {
        Optional<Oeuvre> oeuvre = oeuvreRepository.findById(id);
        if (oeuvre.isPresent()) {
            oeuvre.get().setDelete(true);
            oeuvreRepository.save(oeuvre.get());
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }


    // PARTIE SUR LES EXEMPLAIRES D UNE OEUVRE

    @GetMapping(path="/{oeuvreId}/exemplaire")
    public ResponseEntity<?> getAllExemplaireByOeuvre(@PathVariable("oeuvreId") int oeuvreId, @RequestParam(required = false) Optional<String> type) {
        Iterable<Exemplaire> all;
        if(type.isPresent()){
            all = oeuvreRepository.FindExemplairesPourOeuvre(oeuvreId,"Disponible");
        }else{
            all = oeuvreRepository.findById(oeuvreId).get().getExemplaire();
        }
        return ResponseEntity.ok(all);
    }

    @GetMapping(path="/{oeuvreId}/exemplaire/{exemplaireId}")
    public ResponseEntity<?> getExemplaire(@PathVariable("oeuvreId") int oeuvreId, @PathVariable("exemplaireId") int exemplaireId) {
        Optional<Oeuvre> oeuvre = oeuvreRepository.findById(oeuvreId);
        if(oeuvre.isPresent()){
            Exemplaire exemplaire = oeuvre.get().getExemplaire().stream().
                    filter(p -> p.getId() == exemplaireId).
                    findAny().orElse(null);
            return exemplaire == null
                    ? ResponseEntity.notFound().build()
                    : ResponseEntity.ok(exemplaire);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping(path="/{oeuvreId}/exemplaire")
    @Transactional
    public ResponseEntity<?> addExemplaire (@PathVariable("oeuvreId") int oeuvreId) {
        Optional<Oeuvre> oeuvre = oeuvreRepository.findById(oeuvreId);
        if(oeuvre.isPresent()){
            Oeuvre oeuvre1 = oeuvre.get();
            oeuvre1.ajoutExemplaire(new Exemplaire("Disponible"));
            Oeuvre save = oeuvreRepository.save(oeuvre1);
            Exemplaire ex = save.getExemplaire().get(save.getExemplaire().size() - 1);
            return new ResponseEntity<>(ex, HttpStatus.CREATED);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping(value = "/{oeuvreId}/exemplaire/{exemplaireId}")
    public ResponseEntity<?> updateExemplaire(@RequestBody Exemplaire exemplaire, @PathVariable("oeuvreId") int oeuvreId, @PathVariable("exemplaireId") int exemplaireId) {
        Optional<Oeuvre> oeuvre = oeuvreRepository.findById(oeuvreId);
        if (oeuvre.isPresent()) {
            Exemplaire exemplaire1 = oeuvre.get().getExemplaire().stream().
                    filter(p -> p.getId() == exemplaireId).
                    findAny().orElse(null);
            if(exemplaire1==null){
                return ResponseEntity.notFound().build();
            }else{
                exemplaire1.setEtat(exemplaire.getEtat());
                Exemplaire save = exemplaireRepository.save(exemplaire1);
                return new ResponseEntity<>(save, HttpStatus.OK);
            }
        }
        return ResponseEntity.notFound().build();
    }

    @PatchMapping(path = "/{oeuvreId}/exemplaire/{exemplaireId}", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateExemplaire(@PathVariable("oeuvreId") int oeuvreId, @RequestBody Exemplaire exemplaire, @PathVariable("exemplaireId") int id) {
        Optional<Exemplaire> exemplaire1 = oeuvreRepository.FindExemplaireIdPourOeuvre(oeuvreId, id);
        Optional<Oeuvre> oeuvre = oeuvreRepository.findById(oeuvreId);
        if(exemplaire1.isPresent() && oeuvre.isPresent()){
            if (exemplaire.getEtat() != null) {
                if(exemplaire.getEtat().equals("Bon état")){
                    exemplaire1.get().setEtat("Disponible");
                }else{
                    exemplaire1.get().setEtat(exemplaire.getEtat());
                }
            }
            Exemplaire save = exemplaireRepository.save(exemplaire1.get());
            return new ResponseEntity<>(save, HttpStatus.OK);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping(value = "/{oeuvreId}/exemplaire/{exemplaireId}")
    @Transactional
    public ResponseEntity<?> deleteExemplaire(@PathVariable("oeuvreId") int oeuvreId, @PathVariable("exemplaireId") int exemplaireId) {
        Optional<Exemplaire> exemplaire = oeuvreRepository.FindExemplaireIdPourOeuvre(oeuvreId, exemplaireId);
        if (exemplaire.isPresent()) {
            Exemplaire ex = exemplaire.get();
            ex.setEtat("Supprimé ADMIN");
            exemplaireRepository.save(ex);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

}
