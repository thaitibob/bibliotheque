package com.bibliotheque.controller;

import com.bibliotheque.entity.*;
import com.bibliotheque.repository.OeuvreRepository;
import com.bibliotheque.repository.ReservationRepository;
import com.bibliotheque.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/reservation", produces= MediaType.APPLICATION_JSON_VALUE)
public class ReservationController {

    private final ReservationRepository reservationRepository;
    private final UserRepository userRepository;
    private final OeuvreRepository oeuvreRepository;

    public ReservationController(ReservationRepository reservationRepository, UserRepository userRepository, OeuvreRepository oeuvreRepository) {
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.oeuvreRepository = oeuvreRepository;
    }

    @GetMapping
    public ResponseEntity<?> getAllReservations(@RequestParam(value = "oeuvreId") int oeuvreId) {
        List<Reservation> all = reservationRepository.ReservationByOeuvre(oeuvreId);
        List<ReservationOutput> outputs = new ArrayList<>();
        all.forEach(reservation -> {
            ReservationOutput o1 = new ReservationOutput();
            o1.setId(reservation.getId());
            o1.setDateDebutReservation(reservation.getDateDebutReservation());
            o1.setDateFinReservation(reservation.getDateFinReservation());
            o1.setStatut(reservation.getStatut());
            o1.setId_user(reservation.getUser().getId());
            o1.setPrenom(reservation.getUser().getPrenom());
            o1.setNom(reservation.getUser().getNom());
            o1.setId_oeuvre(reservation.getOeuvre().getId());
            o1.setTitre(reservation.getOeuvre().getTitre());
            o1.setType_oeuvre(reservation.getOeuvre().getType_oeuvre());
            outputs.add(o1);
        });
        return ResponseEntity.ok(outputs);
    }

    @GetMapping(path="/oeuvre/{oeuvreId}")
    public ResponseEntity<?> getByOeuvreAttente(@PathVariable("oeuvreId") int oeuvreId) {
        List<Reservation> all = reservationRepository.ReservationAttenteByOeuvre(oeuvreId);
        List<ReservationOutput> outputs = new ArrayList<>();
        all.forEach(reservation -> {
            ReservationOutput o1 = new ReservationOutput();
            o1.setId(reservation.getId());
            o1.setDateDebutReservation(reservation.getDateDebutReservation());
            o1.setDateFinReservation(reservation.getDateFinReservation());
            o1.setStatut(reservation.getStatut());
            o1.setId_user(reservation.getUser().getId());
            o1.setPrenom(reservation.getUser().getPrenom());
            o1.setNom(reservation.getUser().getNom());
            o1.setId_oeuvre(reservation.getOeuvre().getId());
            o1.setTitre(reservation.getOeuvre().getTitre());
            o1.setType_oeuvre(reservation.getOeuvre().getType_oeuvre());
            outputs.add(o1);
        });
        return ResponseEntity.ok(outputs);
    }

    @GetMapping(path="/user/{userId}")
    public ResponseEntity<?> getReservationUser(@PathVariable("userId") int id) {
        List<Reservation> all = reservationRepository.findByUserId(id);
        List<ReservationOutput> outputs = new ArrayList<>();
        all.forEach(reservation -> {
            ReservationOutput o1 = new ReservationOutput();
            o1.setId(reservation.getId());
            o1.setDateDebutReservation(reservation.getDateDebutReservation());
            o1.setDateFinReservation(reservation.getDateFinReservation());
            o1.setStatut(reservation.getStatut());
            o1.setId_user(reservation.getUser().getId());
            o1.setPrenom(reservation.getUser().getPrenom());
            o1.setNom(reservation.getUser().getNom());
            o1.setId_oeuvre(reservation.getOeuvre().getId());
            o1.setTitre(reservation.getOeuvre().getTitre());
            o1.setType_oeuvre(reservation.getOeuvre().getType_oeuvre());
            outputs.add(o1);
        });
        return ResponseEntity.ok(outputs);
    }

    @PostMapping(path="/user/{userId}/oeuvre/{oeuvreId}")
    @Transactional
    public ResponseEntity<?> addReservation(@PathVariable("userId") int userId, @PathVariable("oeuvreId") int oeuvreId) {
        Optional<User> user = userRepository.findById(userId);
        Optional<Oeuvre> oeuvre = oeuvreRepository.findById(oeuvreId);
        if(user.isPresent() && oeuvre.isPresent()){
            Oeuvre o = oeuvre.get();
            o.setNbReservations(o.getNbReservations()+1);
            oeuvreRepository.save(o);
            Reservation i = new Reservation(oeuvre.get(),user.get());
            Reservation save = reservationRepository.save(i);
            return new ResponseEntity<>(save, HttpStatus.CREATED);
        }
        return ResponseEntity.notFound().build();
    }

    @PatchMapping(path = "/user/{userId}/oeuvre/{oeuvreId}/{reservationId}", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateReservation(@PathVariable("userId") int userId, @RequestBody Reservation reservation, @PathVariable("oeuvreId") int oeuvreId, @PathVariable("reservationId") int reservationId) {
        Optional<Reservation> reservation1 = reservationRepository.FindReservationUserOeuvreResaId(userId, oeuvreId, reservationId);
        Optional<Oeuvre> oeuvre = oeuvreRepository.findById(oeuvreId);
        if(reservation1.isPresent() && oeuvre.isPresent()){
            Oeuvre o = oeuvre.get();
            o.setNbReservations(o.getNbReservations()-1);
            oeuvreRepository.save(o);
            if (reservation.getStatut().equals("Annulée") || reservation.getStatut().equals("Terminée")) {
                reservation1.get().setStatut(reservation.getStatut());
                reservation1.get().setDateFinReservation(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
            }
            Reservation save = reservationRepository.save(reservation1.get());
            return new ResponseEntity<>(save, HttpStatus.OK);
        }
        return ResponseEntity.notFound().build();
    }
}
