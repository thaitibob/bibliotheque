package com.bibliotheque.controller;

import com.bibliotheque.repository.UserRepository;
import com.bibliotheque.entity.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value="/user", produces= MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userDao) {
        this.userRepository = userDao;
    }

    @GetMapping
    public ResponseEntity<?> getAllUsers() {
        Iterable<User> all = userRepository.findAllByDelete(false);
        return ResponseEntity.ok(all);
    }

    @GetMapping(path="/{userId}")
    public ResponseEntity<?> getUser(@PathVariable("userId") int id) {
        Optional<User> user = userRepository.findById(id);
        return user.isEmpty()
                ? ResponseEntity.notFound().build()
                : ResponseEntity.ok(user);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> addUser(@RequestBody User user) {
        User i = new User(user.getNom(), user.getPrenom(), user.getDateNaissance(), user.getAdresse(), user.getNumTel());
        User save = userRepository.save(i);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @PatchMapping(value = "/{userId}")
    @Transactional
    public ResponseEntity<?> updateFieldUser(@RequestBody User user, @PathVariable("userId") int userId) {
        Optional<User> user1 = userRepository.findById(userId);
        if(user1.isPresent()){
            User i = user1.get();
            if(user.getAdresse() != null){
                i.setAdresse(user.getAdresse());
            }
            if(user.getNumTel() != 0){
                i.setNumTel(user.getNumTel());
            }
            User save = userRepository.save(i);
            return new ResponseEntity<>(save, HttpStatus.OK);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping(value = "/{userId}")
    @Transactional
    public ResponseEntity<?> updateUser(@RequestBody User user, @PathVariable("userId") int userId) {
        Optional<User> body = Optional.ofNullable(user);
        if (body.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        if (!userRepository.existsById(userId)) {
            return ResponseEntity.notFound().build();
        }
        user.setId(userId);
        User result = userRepository.save(user);
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{userId}")
    @Transactional
    public ResponseEntity<?> deleteUser(@PathVariable("userId") int clientId) {
        Optional<User> user = userRepository.findById(clientId);
        if (user.isPresent()) {
            user.get().setDelete(true);
            userRepository.save(user.get());
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

}
