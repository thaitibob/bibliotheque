package com.bibliotheque.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Exemplaire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    private String etat;

    @ManyToOne(fetch = FetchType.LAZY)
    private Oeuvre oeuvre;

    @OneToMany(cascade=CascadeType.ALL, orphanRemoval = true, mappedBy = "exemplaire", fetch = FetchType.LAZY)
    private List<Emprunt> emprunts;


    public Exemplaire() { }

    public Exemplaire(String etat) {
        this.etat = etat;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }

    public List<Emprunt> getEmprunts() {
        return emprunts;
    }

    public void setEmprunts(List<Emprunt> emprunts) {
        this.emprunts = emprunts;
    }

    @JsonIgnore
    public Oeuvre getOeuvre() {
        return oeuvre;
    }
}
