package com.bibliotheque.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Livre extends Oeuvre {

    private String edition;

    @Column(columnDefinition = "TEXT")
    private String resume;

    @ManyToMany
    @JoinTable(name="livre_auteur", joinColumns=@JoinColumn(name="oeuvre_id"), inverseJoinColumns=@JoinColumn(name="auteur_id"))
    private List<Auteur> auteurs = new ArrayList<>();

    public Livre() {

    }

    public Livre(String titre, String dateParution, String edition, String resume, String type_oeuvre, List<Auteur> auteur) {
        super(titre, dateParution, type_oeuvre);
        this.edition = edition;
        this.resume = resume;
        this.auteurs = auteur;
    }

    public String getEdition() { return edition; }

    public void setEdition(String edition) { this.edition = edition; }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public List<Auteur> getAuteurs() {
        return auteurs;
    }

    public void setAuteurs(List<Auteur> auteurs) {
        this.auteurs = auteurs;
    }

    public void addAuteur(Auteur a){
        this.auteurs.add(a);
    }

}
