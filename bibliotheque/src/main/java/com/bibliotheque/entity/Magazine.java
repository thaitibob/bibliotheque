package com.bibliotheque.entity;

import javax.persistence.*;

@Entity
public class Magazine extends Oeuvre{

    private int numero;
    private String periodicite;


    public Magazine() { }

    public Magazine(String titre, String dateParution, int numero, String periodicite, String type_oeuvre) {
        super(titre, dateParution, type_oeuvre);
        this.numero = numero;
        this.periodicite = periodicite;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getPeriodicite() {
        return periodicite;
    }

    public void setPeriodicite(String periodicite) {
        this.periodicite = periodicite;
    }

}
