package com.bibliotheque.entity;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy= InheritanceType.JOINED)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type_oeuvre", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Magazine.class, name = "magazine"),
        @JsonSubTypes.Type(value = Livre.class, name = "livre")
})
public class Oeuvre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String type_oeuvre;
    private String titre;
    private String dateParution;
    private int nbReservations;
    @Column(columnDefinition="tinyint(1) default 0")
    private boolean isDelete;

    @OneToMany(cascade=CascadeType.ALL, orphanRemoval = true, mappedBy = "oeuvre", fetch = FetchType.LAZY)
    private List<Exemplaire> exemplaire = new ArrayList<>();

    @OneToMany(cascade=CascadeType.ALL, orphanRemoval = true, mappedBy = "oeuvre", fetch = FetchType.LAZY)
    private List<Reservation> reservations = new ArrayList<>();

    public Oeuvre() {

    }

    public Oeuvre(String titre, String dateParution, String type_oeuvre) {
        this.titre = titre;
        this.dateParution = dateParution;
        this.type_oeuvre = type_oeuvre;
        this.isDelete = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDateParution() {
        return dateParution;
    }

    public void setDateParution(String dateParution) {
        this.dateParution = dateParution;
    }

    public int getNbReservations() {
        return nbReservations;
    }

    public void setNbReservations(int nbReservations) {
        this.nbReservations = nbReservations;
    }

    public String getType_oeuvre() { return type_oeuvre; }

    public void setType_oeuvre(String type_oeuvre) { this.type_oeuvre = type_oeuvre; }

    public List<Exemplaire> getExemplaire() {
        return exemplaire;
    }

    public void setExemplaire(List<Exemplaire> exemplaire) {
        this.exemplaire = exemplaire;
    }

    public void ajoutExemplaire(Exemplaire ex){
        this.exemplaire.add(ex);
        ex.setOeuvre(this);
    }

    public Oeuvre ajoutReservation(Reservation res){
        this.reservations.add(res);
        res.setOeuvre(this);
        return this;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }
}
