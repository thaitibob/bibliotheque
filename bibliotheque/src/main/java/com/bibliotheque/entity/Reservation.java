package com.bibliotheque.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String dateDebutReservation;
    private String dateFinReservation;
    private String statut;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Oeuvre oeuvre;



    public Reservation() { }

    public Reservation(Oeuvre oeuvre, User user) {
        this.user = user;
        this.oeuvre = oeuvre;
        this.dateDebutReservation = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
        this.statut = "En-cours";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateDebutReservation() {
        return dateDebutReservation;
    }

    public void setDateDebutReservation(String dateDebutReservation) { this.dateDebutReservation = dateDebutReservation; }

    public String getDateFinReservation() {
        return dateFinReservation;
    }

    public void setDateFinReservation(String dateFinReservation) {
        this.dateFinReservation = dateFinReservation;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    @JsonIgnore
    public User getUser(){
        return user;
    }

    @JsonIgnore
    public Oeuvre getOeuvre(){
        return oeuvre;
    }
}
