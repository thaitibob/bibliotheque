package com.bibliotheque.entity;


public class ReservationOutput {

    private int id;
    private String dateDebutReservation;
    private String dateFinReservation;
    private String statut;
    private int id_user;
    private String nom;
    private String prenom;
    private int id_oeuvre;
    private String titre;
    private String type_oeuvre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDateDebutReservation() {
        return dateDebutReservation;
    }

    public void setDateDebutReservation(String dateDebutReservation) {
        this.dateDebutReservation = dateDebutReservation;
    }

    public String getDateFinReservation() {
        return dateFinReservation;
    }

    public void setDateFinReservation(String dateFinReservation) {
        this.dateFinReservation = dateFinReservation;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_oeuvre() {
        return id_oeuvre;
    }

    public void setId_oeuvre(int id_oeuvre) {
        this.id_oeuvre = id_oeuvre;
    }

    public String getType_oeuvre() {
        return type_oeuvre;
    }

    public void setType_oeuvre(String type_oeuvre) {
        this.type_oeuvre = type_oeuvre;
    }
}
