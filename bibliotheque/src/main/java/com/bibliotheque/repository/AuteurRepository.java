package com.bibliotheque.repository;

import com.bibliotheque.entity.Auteur;
import com.bibliotheque.entity.Exemplaire;
import com.bibliotheque.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AuteurRepository extends CrudRepository <Auteur, Integer> {

    @Query("select A from Auteur A join A.livres livres where livres.id = ?1")
    List<Auteur> FindAuteursPourLivre(int id_livre);

    @Query("select A from Auteur A where A.isDelete = ?1")
    Iterable<Auteur> findAllByDelete(boolean id);
}
