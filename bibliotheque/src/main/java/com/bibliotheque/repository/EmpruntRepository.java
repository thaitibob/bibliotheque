package com.bibliotheque.repository;

import com.bibliotheque.entity.Emprunt;

import com.bibliotheque.entity.Reservation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface EmpruntRepository extends CrudRepository<Emprunt, Integer> {

    @Query("select E FROM Emprunt E inner join Exemplaire Ex on Ex.Id = E.exemplaire.Id inner join User U on U.id = E.user.id where U.id = ?1")
    List<Emprunt> findByUserId(int id);

    @Query("select E FROM Emprunt E inner join Exemplaire Ex on Ex.Id = E.exemplaire.Id inner join User U on U.id = E.user.id inner join Oeuvre O on O.id = Ex.oeuvre.id where U.id = ?1")
    Optional<Reservation> FindReservationUserOeuvreResaId(int id_u, int id_ex, int id_em);

    @Query("select E from Emprunt E inner join Exemplaire Ex on Ex.Id = E.exemplaire.Id inner join Oeuvre O on O.id = Ex.oeuvre.id where O.id = ?1")
    List<Emprunt> EmpruntByOeuvre(int id_o);

}
