package com.bibliotheque.repository;

import com.bibliotheque.entity.Exemplaire;
import org.springframework.data.repository.CrudRepository;

public interface ExemplaireRepository extends CrudRepository<Exemplaire, Integer> {

}
