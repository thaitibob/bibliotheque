package com.bibliotheque.repository;

import com.bibliotheque.entity.Livre;
import org.springframework.data.repository.CrudRepository;

public interface LivreRepository extends CrudRepository<Livre,Integer> {
}
