package com.bibliotheque.repository;

import com.bibliotheque.entity.Magazine;
import org.springframework.data.repository.CrudRepository;

public interface MagazineRepository extends CrudRepository<Magazine, Integer> {

}
