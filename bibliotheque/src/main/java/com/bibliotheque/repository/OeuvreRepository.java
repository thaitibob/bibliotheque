package com.bibliotheque.repository;

import com.bibliotheque.entity.Auteur;
import com.bibliotheque.entity.Exemplaire;
import com.bibliotheque.entity.Oeuvre;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface OeuvreRepository extends CrudRepository<Oeuvre, Integer> {

    @Query("select ex from Exemplaire ex inner join Oeuvre O on O.id = ex.oeuvre.id where O.id = ?1 and ex.etat = ?2")
    Iterable<Exemplaire> FindExemplairesPourOeuvre(int id,String etat);

    @Query("select ex from Exemplaire ex inner join Oeuvre O on O.id = ex.oeuvre.id where O.id = ?1  and ex.Id = ?2")
    Optional<Exemplaire> FindExemplaireIdPourOeuvre(int id, int id_ex);

    @Query("select ex from Exemplaire ex inner join Oeuvre O on O.id = ex.oeuvre.id where O.id = ?1  and ex.Id = ?2 and ex.etat = ?3")
    Optional<Exemplaire> FindExemplaireIdPourOeuvreStatut(int id, int id_ex, String statut);

    @Query("select O from Oeuvre O where O.isDelete = ?1")
    Iterable<Oeuvre> findAllByDelete(boolean id);

    @Query("select O from Oeuvre O join Livre l on l.id = O.id join l.auteurs A where A.id = ?1")
    List<Oeuvre> FindLivrePourAuteur(int id_auteur);

}
