package com.bibliotheque.repository;

import com.bibliotheque.entity.Reservation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends CrudRepository<Reservation, Integer> {

    @Query("select R FROM Reservation R inner join Oeuvre O on O.id = R.oeuvre.id inner join User U on U.id = R.user.id where U.id = ?1")
    List<Reservation> findByUserId(int id);

    @Query("select R from Reservation R inner join Oeuvre O on O.id = R.oeuvre.id inner join User U on U.id = R.user.id where U.id = ?1 and O.id = ?2  and R.id = ?3")
    Optional<Reservation> FindReservationUserOeuvreResaId(int id_u, int id_o, int id_r);

    @Query("select R from Reservation R inner join Oeuvre O on O.id = R.oeuvre.id where O.id = ?1 and R.dateFinReservation is null")
    List<Reservation> ReservationAttenteByOeuvre(int id_o);

    @Query("select R from Reservation R inner join Oeuvre O on O.id = R.oeuvre.id where O.id = ?1")
    List<Reservation> ReservationByOeuvre(int id_o);
}
