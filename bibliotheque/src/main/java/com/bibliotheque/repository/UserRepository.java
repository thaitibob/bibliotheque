package com.bibliotheque.repository;

import com.bibliotheque.entity.Oeuvre;
import com.bibliotheque.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {

    @Query("select U from User U where U.isDelete = ?1")
    Iterable<User> findAllByDelete(boolean id);

}
