INSERT INTO user (adresse, date_naissance, nom, num_tel, prenom) VALUES ('4 rue de la fontaine','19-06-1995','valle',9999,'Jonathan');
INSERT INTO user (adresse, date_naissance, nom, num_tel, prenom) VALUES ('7 avenue du palmier','19/06/1995','flute',9999,'Michel');
INSERT INTO user (adresse, date_naissance, nom, num_tel, prenom) VALUES ('4 route du chemin','19/06/1995','haricot',9999,'Gérard');
INSERT INTO user (adresse, date_naissance, nom, num_tel, prenom) VALUES ('12 boulevard matignon','19/06/1995','Truc',9999,'Macron');
INSERT INTO user (adresse, date_naissance, nom, num_tel, prenom) VALUES ('99 place robert','15-11-1999','Machin',9999,'Brice');
INSERT INTO user (adresse, date_naissance, nom, num_tel, prenom) VALUES ('4 plop','15-11-1998','Bidule',9999,'Clément');


INSERT INTO oeuvre (date_parution, nb_reservations, titre, type_oeuvre) VALUES ('15-07-2020',0,'Marcel Pagnol','livre');
INSERT INTO livre (edition, resume, id) VALUES ('27-02-2020','Vivamus tristique urna sit amet tortor fringilla commodo. Ut tincidunt turpis sem, ornare congue magna molestie ac. Duis hendrerit consequat erat sed mattis. Aliquam a venenatis nisi. Curabitur vitae commodo nulla, nec aliquet enim. Curabitur convallis in tortor eu scelerisque. Vestibulum eget velit vitae nibh lacinia viverra. Nam rhoncus neque efficitur purus pellentesque euismod nec egestas massa. Vestibulum id turpis tempor, finibus nisl a, suscipit magna. Nulla id quam et diam blandit ornare. Mauris posuere vel nulla ac gravida. Sed facilisis, elit id feugiat interdum, mi ipsum sollicitudin odio, eget accumsan metus diam vitae nulla. Vivamus sit amet sollicitudin sem, bibendum volutpat lorem.',1);

INSERT INTO oeuvre (date_parution, nb_reservations, titre, type_oeuvre) VALUES ('12-06-2015',0,'Babar','livre');
INSERT INTO livre (edition, resume, id) VALUES ('25-12-2020','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dignissim, lorem quis dapibus laoreet, lorem neque tempor turpis, in varius tortor justo tempus nibh. Etiam finibus gravida risus, at dictum nulla bibendum ut. Nam non augue sed erat scelerisque lobortis. Quisque venenatis porta pretium. Integer venenatis neque posuere lorem tempor, ut semper orci lobortis. Curabitur tempus turpis feugiat scelerisque egestas. Pellentesque ultricies consectetur ullamcorper. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed mattis augue id fermentum commodo.',2);

INSERT INTO oeuvre (date_parution, nb_reservations, titre, type_oeuvre) VALUES ('01-02-2000',0,'Les 3 mousquetaires','livre');
INSERT INTO livre (edition, resume, id) VALUES ('27-01-2020','Praesent eu massa mollis, mollis nibh sed, venenatis sapien. Vivamus imperdiet augue vel turpis faucibus iaculis. Phasellus sodales elit ligula, id sodales nulla venenatis vitae. Aliquam sit amet vulputate magna. Nullam felis quam, auctor vel tortor vitae, pretium cursus est. Mauris finibus ultricies dapibus. Donec aliquet dictum ullamcorper.',3);

INSERT INTO oeuvre (date_parution, nb_reservations, titre, type_oeuvre) VALUES ('05-09-1980',0,'La Bible','livre');
INSERT INTO livre (edition, resume, id) VALUES ('18-07-2020','Suspendisse dignissim sed tortor quis pellentesque. Nullam a erat et nibh sagittis facilisis sed vitae mi. Duis vitae hendrerit nulla. Quisque quis diam lectus. Etiam euismod volutpat finibus. Aenean condimentum malesuada ex, ac euismod quam imperdiet at. Aenean suscipit nisi at ligula malesuada, ac posuere nisi fermentum. Aliquam ornare nibh in leo fermentum viverra. Etiam non urna eu risus blandit viverra quis eget dolor. Morbi nibh leo, cursus ut odio et, porta posuere lectus. Etiam varius justo quis nisl vestibulum elementum. Phasellus porta, velit at vestibulum dignissim, turpis enim mattis turpis, nec egestas ex est non urna. Cras scelerisque, elit et scelerisque lobortis, mauris urna consequat nunc, et consequat augue ',4);

INSERT INTO oeuvre (date_parution, nb_reservations, titre, type_oeuvre) VALUES ('27-12-1990',0,'Un sac de billes','livre');
INSERT INTO livre (edition, resume, id) VALUES ('12-09-2020','Praesent eleifend nibh tellus, vitae pulvinar tortor venenatis ac. Integer non eros aliquam orci aliquet pharetra sed ut magna. Phasellus a magna a lectus mattis pellentesque. Duis sem orci, mattis nec felis a, molestie vulputate erat. Duis ligula tortor, ullamcorper vitae posuere nec, fringilla at dui. In lectus est, mollis ut nulla sed, pharetra aliquet neque. Cras placerat nibh id orci ornare semper. Pellentesque ac molestie tortor, id interdum sapien. Duis dictum ut risus eget accumsan. Integer condimentum tempor ultrices. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent mattis, neque id luctus egestas, ipsum massa hendrerit libero, ut tempus leo nisl sed ante. Vestibulum varius ante sit amet eros scelerisque gravida. Sed dictum nulla et risus consequat pharetra.',5);

INSERT INTO oeuvre (date_parution, nb_reservations, titre, type_oeuvre) VALUES ('25-02-2020',0,'Closer','magazine');
INSERT INTO magazine (numero, periodicite, id) VALUES (10,'Journalier',6);

INSERT INTO oeuvre (date_parution, nb_reservations, titre, type_oeuvre) VALUES ('12-04-2020',0,'Public','magazine');
INSERT INTO magazine (numero, periodicite, id) VALUES (3,'Hebdomadaire',7);

INSERT INTO oeuvre (date_parution, nb_reservations, titre, type_oeuvre) VALUES ('15-06-2020',0,'Sciences et Avenir','magazine');
INSERT INTO magazine (numero, periodicite, id) VALUES (5,'Mensuel',8);

INSERT INTO oeuvre (date_parution, nb_reservations, titre, type_oeuvre) VALUES ('18-09-2020',0,'Télé 7 jours','magazine');
INSERT INTO magazine (numero, periodicite, id) VALUES (6,'Annuel',9);

INSERT INTO oeuvre (date_parution, nb_reservations, titre, type_oeuvre) VALUES ('13-12-2020',0,'Femme Actuelle','magazine');
INSERT INTO magazine (numero, periodicite, id) VALUES (8,'Journalier',10);

INSERT INTO auteur (nom, prenom) VALUES ('Michelin','Guide');
INSERT INTO auteur (nom, prenom) VALUES ('Franck','Ribéry');
INSERT INTO auteur (nom, prenom) VALUES ('Mbappe','Kylian');
INSERT INTO auteur (nom, prenom) VALUES ('Michelin','Guide');
INSERT INTO auteur (nom, prenom) VALUES ('Michelin','Guide');

INSERT INTO livre_auteur(oeuvre_id,auteur_id) VALUES (1,1);
INSERT INTO livre_auteur(oeuvre_id,auteur_id) VALUES (1,2);
INSERT INTO livre_auteur(oeuvre_id,auteur_id) VALUES (2,3);
INSERT INTO livre_auteur(oeuvre_id,auteur_id) VALUES (2,4);
INSERT INTO livre_auteur(oeuvre_id,auteur_id) VALUES (3,5);
INSERT INTO livre_auteur(oeuvre_id,auteur_id) VALUES (4,1);
INSERT INTO livre_auteur(oeuvre_id,auteur_id) VALUES (5,1);
