package com.clientui.beans;

import org.springframework.stereotype.Component;

@Component
public class ExemplaireBean {


    private int Id;
    private String etat;

    public ExemplaireBean(String etat) {
        this.etat = etat;
    }

    public ExemplaireBean() {

    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

}
