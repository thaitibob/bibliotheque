package com.clientui.beans;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LivreBean extends Oeuvre {

    private String edition;
    private String resume;
    private List<Auteur> auteur = new ArrayList<>();

    public LivreBean() {

    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public List<Auteur> getAuteur() {
        return auteur;
    }

    public void setAuteur(List<Auteur> auteur) {
        this.auteur = auteur;
    }
}
