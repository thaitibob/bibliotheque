package com.clientui.beans;

import org.springframework.stereotype.Component;

@Component
public class MagazineBean extends Oeuvre {

    private int numero;
    private String periodicite;

    public MagazineBean() {

    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getPeriodicite() {
        return periodicite;
    }

    public void setPeriodicite(String periodicite) {
        this.periodicite = periodicite;
    }
}
