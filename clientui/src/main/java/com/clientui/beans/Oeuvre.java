package com.clientui.beans;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type_oeuvre", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = MagazineBean.class, name = "magazine"),
        @JsonSubTypes.Type(value = LivreBean.class, name = "livre")
})
public class Oeuvre {

    private int id;
    private String type_oeuvre;
    private String titre;
    private String dateParution;
    private int nbReservations;
    private boolean isDelete;

    private List<ExemplaireBean> exemplaire = new ArrayList<>();

    public Oeuvre() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDateParution() {
        return dateParution;
    }

    public void setDateParution(String dateParution) {
        this.dateParution = dateParution;
    }

    public int getNbReservations() {
        return nbReservations;
    }

    public void setNbReservations(int nbReservations) {
        this.nbReservations = nbReservations;
    }

    public String getType_oeuvre() { return type_oeuvre; }

    public void setType_oeuvre(String type_oeuvre) { this.type_oeuvre = type_oeuvre; }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

}
