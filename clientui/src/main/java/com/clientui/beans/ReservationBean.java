package com.clientui.beans;

import org.springframework.stereotype.Component;

@Component
public class ReservationBean {

    private int id;
    private String dateDebutReservation;
    private String dateFinReservation;
    private String statut;
    private UserBean user;
    private Oeuvre oeuvre;


    public ReservationBean() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateDebutReservation() {
        return dateDebutReservation;
    }

    public void setDateDebutReservation(String dateDebutReservation) {
        this.dateDebutReservation = dateDebutReservation;
    }

    public String getDateFinReservation() {
        return dateFinReservation;
    }

    public void setDateFinReservation(String dateFinReservation) {
        this.dateFinReservation = dateFinReservation;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public Oeuvre getOeuvre() {
        return oeuvre;
    }

    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }
}
