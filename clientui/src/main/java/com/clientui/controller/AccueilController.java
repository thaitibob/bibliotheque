package com.clientui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AccueilController {

    @RequestMapping("/")
    public String accueil(){
        return "Accueil";
    }
}
