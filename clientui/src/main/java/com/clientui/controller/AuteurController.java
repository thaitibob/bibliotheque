package com.clientui.controller;

import com.clientui.beans.*;
import com.clientui.proxies.MicroserviceBibliothequeProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/auteur")
public class AuteurController {

    @Autowired
    private MicroserviceBibliothequeProxy BibliothequeProxy;

    @PostMapping
    public ResponseEntity<?> ajoutBddAuteur (@RequestBody Auteur auteur){
        Auteur save = BibliothequeProxy.addAuteur(auteur);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @PostMapping(value = "{auteurid}/livre/{livreId}")
    public ResponseEntity<?> addAuteurLivre(@PathVariable("auteurid") int auteurid, @PathVariable("livreId") int livreId) {
        Auteur save = BibliothequeProxy.addAuteurLivre(auteurid, livreId);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @GetMapping
    public String rechercheAuteur(@ModelAttribute Auteur auteur, Model model){
        Iterable<Auteur> auteurs =  BibliothequeProxy.listeAuteurs();
        model.addAttribute("auteurs", auteurs);
        return "auteur/listeAuteurs";
    }

    @GetMapping("/{auteurId}")
    public String infoAuteur(@PathVariable("auteurId") int id, @ModelAttribute Auteur auteur, Model model){
        Auteur auteurById = BibliothequeProxy.auteurById(id);
        List<Oeuvre> oeuvres = BibliothequeProxy.listeLivreByAuteurs(id);
        model.addAttribute("oeuvres", oeuvres);
        model.addAttribute("auteur",auteurById);
        if(auteurById.isDelete()){
            return "auteur/auteurSupp";
        }
        return "auteur/infoAuteur";
    }

    //On range le HTML d'ajout d'auteur dans le dossier oeuvre car cela est plus cohérent
    @GetMapping("/ajout")
    public String ajoutAuteur(){
        return "auteur/ajoutAuteur";
    }


    @PutMapping("/{auteurId}")
    public ResponseEntity<?> updateAuteur(@RequestBody Auteur auteur, @PathVariable("auteurId") int auteurId){
        Auteur save = BibliothequeProxy.updateAuteur(auteur,auteurId);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @DeleteMapping("/{auteurId}")
    public ResponseEntity<?> deleteAuteur(@PathVariable("auteurId") int id){
        ResponseEntity<?> responseEntity = BibliothequeProxy.deleteAuteur(id);
        return responseEntity;
    }




}
