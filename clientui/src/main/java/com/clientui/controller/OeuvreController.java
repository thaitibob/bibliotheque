package com.clientui.controller;

import com.clientui.beans.*;
import com.clientui.proxies.MicroserviceBibliothequeProxy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Controller
@RequestMapping(path="/oeuvre")
public class OeuvreController {

    private final MicroserviceBibliothequeProxy BibliothequeProxy;

    public OeuvreController(MicroserviceBibliothequeProxy BibliothequeProxy) {
        this.BibliothequeProxy = BibliothequeProxy;
    }

    @GetMapping
    public String listeDesOeuvres(Model model){
        List<Oeuvre> oeuvre =  BibliothequeProxy.listeDesOeuvres();
        model.addAttribute("oeuvre", oeuvre);
        return "oeuvre/rechercheOeuvre";
    }

    @GetMapping("/ajoutMagazine")
    public String ajoutMagazine(){
        return "oeuvre/ajoutMagazine";
    }

    @GetMapping("/ajoutLivre")
    public String ajoutLivre(Model model){
        Iterable<Auteur> auteurs = BibliothequeProxy.listeAuteurs();
        model.addAttribute("auteurs", auteurs);
        return "oeuvre/ajoutLivre";
    }

    @GetMapping("/{oeuvreId}")
    public String infoOeuvre(@PathVariable("oeuvreId") int oeuvreId, @ModelAttribute Oeuvre oeuvre, Model model) {
        List<ExemplaireBean> exemplaire = BibliothequeProxy.listeDesExemplaires(oeuvreId);
        List<ReservationOutputBean> reservations = BibliothequeProxy.getReservationByOeuvre(oeuvreId);
        List<EmpruntOutPutBean> emprunts = BibliothequeProxy.getEmpruntsByOeuvre(oeuvreId);
        Oeuvre oeuvre1 = BibliothequeProxy.oeuvreById(oeuvreId);
        AtomicBoolean pas_emprunt_en_cours = new AtomicBoolean(true);
        AtomicBoolean pas_reservation_en_cours = new AtomicBoolean(true);
        emprunts.forEach((e) -> {
            if (e.getDateRendu() == null) pas_emprunt_en_cours.set(false);
        });
        reservations.forEach((r) -> {
            if (r.getDateFinReservation() == null) pas_reservation_en_cours.set(false);
        });
        model.addAttribute("exemplaire", exemplaire);
        model.addAttribute("reservations",reservations);
        model.addAttribute("emprunts",emprunts);
        model.addAttribute("pas_emprunt_en_cours",pas_emprunt_en_cours.get());
        model.addAttribute("pas_reservation_en_cours",pas_reservation_en_cours.get());

        if(oeuvre1.isDelete()){
            return "oeuvre/oeuvreSupp";
        }
        switch (oeuvre1.getType_oeuvre()) {
            case "livre":
                List<Auteur> auteurs = BibliothequeProxy.listeAuteursByLivre(oeuvreId);
                model.addAttribute("auteurs", auteurs);
                model.addAttribute("livre", oeuvre1);
                return "oeuvre/infoLivre";
            case "magazine":
                model.addAttribute("magazine", oeuvre1);
                return "oeuvre/infoMagazine";
            default:
                return "";
        }
    }

    @PostMapping(produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> ajoutBddOeuvre(@RequestBody Oeuvre oeuvre, @ModelAttribute Oeuvre oeuvre1, Model model){
        Oeuvre save = BibliothequeProxy.ajouterOeuvre(oeuvre);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @PatchMapping(value = "/{oeuvreId}")
    public ResponseEntity<?> updateLivre(@RequestBody Oeuvre oeuvre, @PathVariable("oeuvreId") int oeuvreId){
        Oeuvre save = BibliothequeProxy.updateOeuvre(oeuvre,oeuvreId);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @DeleteMapping("/{oeuvreId}")
    public ResponseEntity<?> deleteOeuvre(@PathVariable("oeuvreId") int id){
        ResponseEntity<?> responseEntity = BibliothequeProxy.deleteOeuvre(id);
        List<ExemplaireBean> exemplaire = BibliothequeProxy.listeDesExemplaires(id);
        exemplaire.forEach((ex) -> {
            BibliothequeProxy.deleteExemplaire(id, ex.getId());
        });
        return responseEntity;
    }

    @DeleteMapping("/{oeuvreId}/exemplaire/{exemplaireId}")
    public ResponseEntity<?> deleteExemplaire(@PathVariable("oeuvreId") int id, @PathVariable("exemplaireId") int exemplaireId){
        ResponseEntity<?> responseEntity = BibliothequeProxy.deleteExemplaire(id, exemplaireId);
        return responseEntity;
    }

    // GESTION DES EXEMPLAIRES

    @GetMapping("/{oeuvreId}/exemplaire")
    public ResponseEntity<?> listeDesExemplaires(@PathVariable("oeuvreId") int id, Model model, @RequestParam(required = false) Optional<String> type){
        List<ExemplaireBean> exemplaires;
        if(type.isPresent()){
            exemplaires = BibliothequeProxy.listeDesExemplairesParam(id,type);
        }else{
            exemplaires = BibliothequeProxy.listeDesExemplaires(id);
        }
        return ResponseEntity.ok(exemplaires);
    }

    @PostMapping(path = "/{oeuvreId}/exemplaire", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addExemplaire(@PathVariable("oeuvreId") int id, @RequestParam("nb_exemplaire") int nb){
        ExemplaireBean save = null;
        for (int i=0 ; i<nb ; i++){
            save = BibliothequeProxy.addExemplaire(id);
            List<ReservationOutputBean> reservation = BibliothequeProxy.ReservationByOeuvre(id);
            // on check si y a des réservation qui peuvent aboutir à un emprunt
            if(reservation.size()>0){
                ReservationOutputBean r = reservation.get(0);
                ReservationBean r1 = new ReservationBean();
                r1.setStatut("Terminée");
                BibliothequeProxy.updateReservation(r.getId_user(), r1, id, r.getId());
                EmpruntBean e1 = new EmpruntBean();
                e1.setDureeEmprunt(7);
                BibliothequeProxy.addEmprunt(r.getId_user(), id, save.getId(), e1);
            }
        }
        return new ResponseEntity<>(save, HttpStatus.OK);
    }


}
