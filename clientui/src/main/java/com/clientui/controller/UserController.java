package com.clientui.controller;

import com.clientui.beans.*;
import com.clientui.proxies.MicroserviceBibliothequeProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Controller
@RequestMapping(path="/user")
public class UserController {
    @Autowired
    private MicroserviceBibliothequeProxy BibliothequeProxy;

    @GetMapping
    public String rechercheClient(@ModelAttribute UserBean user, Model model){
        List<UserBean> users =  BibliothequeProxy.listeDesUsers();
        model.addAttribute("users", users);
        return "user/rechercheUser";
    }

    @GetMapping("/ajout")
    public String ajoutUser(){
        return "user/ajoutUser";
    }

    @PostMapping
    public ResponseEntity<?> ajoutBddUser(@RequestBody UserBean user){
        UserBean save = BibliothequeProxy.ajouterUser(user);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @GetMapping("/{userId}")
    public String infoClient(@PathVariable("userId") int id, @ModelAttribute UserBean user, Model model){
        UserBean userbyId = BibliothequeProxy.userById(id);
        List<Oeuvre> oeuvre = BibliothequeProxy.listeDesOeuvres();
        List<EmpruntOutPutBean> emprunts = BibliothequeProxy.getEmpruntUser(id);
        List<ReservationOutputBean> reservations = BibliothequeProxy.getReservationUser(id);

        AtomicBoolean pas_emprunt_en_cours = new AtomicBoolean(true);
        AtomicBoolean pas_reservation_en_cours = new AtomicBoolean(true);
        emprunts.forEach((e) -> {
            if (e.getDateRendu() == null) pas_emprunt_en_cours.set(false);
        });
        reservations.forEach((r) -> {
            if (r.getDateFinReservation() == null) pas_reservation_en_cours.set(false);
        });

        model.addAttribute("pas_emprunt_en_cours",pas_emprunt_en_cours.get());
        model.addAttribute("pas_reservation_en_cours",pas_reservation_en_cours.get());
        model.addAttribute("user",userbyId);
        model.addAttribute("oeuvre",oeuvre);
        model.addAttribute("reservations",reservations);
        model.addAttribute("emprunts",emprunts);
        if(userbyId.isDelete()){
            return "user/userSupp";
        }
        return "user/infoUser";
    }

    @PatchMapping("/{userId}")
    public ResponseEntity<?> updateUser(@RequestBody UserBean user, @PathVariable("userId") int userId){
        UserBean save = BibliothequeProxy.updateUser(user,userId);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable("userId") int id){
        ResponseEntity<?> responseEntity = BibliothequeProxy.deleteUser(id);
        return responseEntity;
    }

    @PostMapping("/{userId}/oeuvre/{oeuvreId}/reservation")
    public ResponseEntity<?> addReservation(@PathVariable("userId") int userId, @PathVariable("oeuvreId") int oeuvreId){
        ReservationBean save = BibliothequeProxy.addReservation(userId, oeuvreId);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @PostMapping("/{userId}/oeuvre/{oeuvreId}/exemplaire/{exemplaireId}/emprunt")
    public ResponseEntity<?> addEmprunt(@PathVariable("userId") int userId, @PathVariable("oeuvreId") int oeuvreId, @PathVariable("exemplaireId") int exemplaireId, @RequestBody EmpruntBean emprunt){
        ReservationBean save = BibliothequeProxy.addEmprunt(userId, oeuvreId, exemplaireId, emprunt);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @PatchMapping("/{userId}/oeuvre/{oeuvreId}/reservation/{reservationId}")
    public ResponseEntity<?> updateReservation(@PathVariable("userId") int userId, @RequestBody ReservationBean reservation, @PathVariable("oeuvreId") int oeuvreId, @PathVariable("reservationId") int reservationId){
        ReservationBean save = BibliothequeProxy.updateReservation(userId, reservation, oeuvreId, reservationId);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @GetMapping("/{userId}/{empruntId}")
    public String detailEmprunt(@PathVariable("userId") int userId, @PathVariable("empruntId") int empruntId, Model model){
        EmpruntOutPutBean emprunt = BibliothequeProxy.getEmpruntByIdAndUser(userId, empruntId);
        model.addAttribute("emprunt", emprunt);
        return "user/detailEmprunt";
    }

    @PatchMapping("/{userId}/oeuvre/{oeuvreId}/exemplaire/{exemplaireId}/{empruntId}")
    public ResponseEntity<?> updateEmprunt(@RequestBody EmpruntOutPutBean emprunt,
                                           @PathVariable("userId") int userId,
                                           @PathVariable("oeuvreId") int oeuvreId,
                                           @PathVariable("exemplaireId") int exemplaireId,
                                           @PathVariable("empruntId") int empruntId){
        EmpruntBean save = BibliothequeProxy.updateEmprunt(userId, oeuvreId, exemplaireId, empruntId, emprunt);
        ExemplaireBean e = new ExemplaireBean();
        e.setEtat(emprunt.getEtat());

        BibliothequeProxy.updateExemplaire(oeuvreId, e, exemplaireId);
        if(emprunt.getEtat().equals("Bon état")){
            List<ReservationOutputBean> reservation = BibliothequeProxy.ReservationByOeuvre(oeuvreId);
            // on check si y a des réservation qui peuvent aboutir à un emprunt
            if(reservation.size()>0){
                ReservationOutputBean r = reservation.get(0);
                ReservationBean r1 = new ReservationBean();
                r1.setStatut("Terminée");
                BibliothequeProxy.updateReservation(r.getId_user(), r1, oeuvreId, r.getId());
                EmpruntBean e1 = new EmpruntBean();
                e1.setDureeEmprunt(7);
                BibliothequeProxy.addEmprunt(r.getId_user(), oeuvreId, exemplaireId, e1);
            }
        }
        return new ResponseEntity<>(save, HttpStatus.OK);
    }
}
