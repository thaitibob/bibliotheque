package com.clientui.proxies;

import com.clientui.beans.*;
import com.clientui.config.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@FeignClient(name = "microservice-bibliotheque", url = "api-bibliotheque:9090", configuration = FeignConfig.class)
public interface MicroserviceBibliothequeProxy {

    // User

    @GetMapping(value = "/user")
    List<UserBean> listeDesUsers();

    @GetMapping(value = "/user/{userId}")
    UserBean userById(@PathVariable("userId") int userId);

    @PostMapping(path = "/user")
    UserBean ajouterUser(@RequestBody UserBean user);

    @PatchMapping(value = "/user/{userId}")
    UserBean updateUser(@RequestBody UserBean user, @PathVariable("userId") int userId);

    @DeleteMapping(value = "/user/{userId}")
    ResponseEntity<?> deleteUser(@PathVariable("userId") int userId);

    // Oeuvre

    @GetMapping(value = "/oeuvre")
    List<Oeuvre> listeDesOeuvres();

    @GetMapping(value = "/oeuvre/{oeuvreId}")
    Oeuvre oeuvreById(@PathVariable("oeuvreId") int oeuvreId);

    @PostMapping(path = "/oeuvre")
    Oeuvre ajouterOeuvre(@RequestBody Oeuvre oeuvre);

    @PatchMapping(value = "/oeuvre/{oeuvreId}")
    Oeuvre updateOeuvre(@RequestBody Oeuvre oeuvre, @PathVariable("oeuvreId") int Id);

    @DeleteMapping(value = "/oeuvre/{oeuvreId}")
    ResponseEntity<?> deleteOeuvre(@PathVariable("oeuvreId") int oeuvreId);

    //EXEMPLAIRE

    @GetMapping(value = "/oeuvre/{oeuvreId}/exemplaire")
    List<ExemplaireBean> listeDesExemplaires(@PathVariable("oeuvreId") int oeuvreId);

    @GetMapping(value = "/oeuvre/{oeuvreId}/exemplaire")
    List<ExemplaireBean> listeDesExemplairesParam(@PathVariable("oeuvreId") int oeuvreId, @RequestParam(required = false) Optional<String> type);

    @GetMapping(value = "/oeuvre/{oeuvreId}/exemplaire/{exemplaireId}")
    ExemplaireBean listeExemplairesById(@PathVariable("oeuvreId") int oeuvreId, @PathVariable("exemplaireId") int exemplaireId);

    @PostMapping(value = "/oeuvre/{oeuvreId}/exemplaire")
    ExemplaireBean addExemplaire(@PathVariable("oeuvreId") int oeuvreId);

    @PutMapping(value = "/oeuvre/{oeuvreId}/exemplaire/{exemplaireId}")
    ExemplaireBean updateExemplaires(@PathVariable("oeuvreId") int oeuvreId, @PathVariable("exemplaireId") int exemplaireId);

    @PatchMapping(path = "/oeuvre/{oeuvreId}/exemplaire/{exemplaireId}", produces= MediaType.APPLICATION_JSON_VALUE)
    ExemplaireBean updateExemplaire(@PathVariable("oeuvreId") int oeuvreId, @RequestBody ExemplaireBean exemplaire, @PathVariable("exemplaireId") int id);

    @DeleteMapping (value = "/oeuvre/{oeuvreId}/exemplaire/{exemplaireId}")
    ResponseEntity<?> deleteExemplaire(@PathVariable("oeuvreId") int oeuvreId, @PathVariable("exemplaireId") int exemplaireId);

    // Reservation

    @GetMapping(value = "/reservation")
    List<ReservationOutputBean> getReservationByOeuvre(@RequestParam(value = "oeuvreId") int id);

    @GetMapping(value = "/reservation/user/{userId}")
    List<ReservationOutputBean> getReservationUser(@PathVariable("userId") int userId);

    @GetMapping(value = "/reservation/oeuvre/{oeuvreId}")
    List<ReservationOutputBean> ReservationByOeuvre(@PathVariable("oeuvreId") int oeuvreId);

    @PostMapping(value = "/reservation/user/{userId}/oeuvre/{oeuvreId}")
    ReservationBean addReservation(@PathVariable("userId") int userId, @PathVariable("oeuvreId") int oeuvreId);

    @PatchMapping(value = "/reservation/user/{userId}/oeuvre/{oeuvreId}/{reservationId}")
    ReservationBean updateReservation(@PathVariable("userId") int userId, @RequestBody ReservationBean reservation, @PathVariable("oeuvreId") int oeuvreId, @PathVariable("reservationId") int reservationId);

    // Emprunt

    @GetMapping(value = "/emprunt")
    List<EmpruntOutPutBean> getEmpruntsByOeuvre(@RequestParam(value = "oeuvreId") int id);

    @GetMapping(value = "/emprunt/user/{userId}/{empruntId}")
    EmpruntOutPutBean getEmpruntByIdAndUser(@PathVariable("userId") int userId, @PathVariable("empruntId") int empruntId);

    @GetMapping(value = "/emprunt/user/{userId}")
    List<EmpruntOutPutBean> getEmpruntUser(@PathVariable("userId") int userId);

    @PostMapping(value = "/emprunt/user/{userId}/oeuvre/{oeuvreId}/exemplaire/{exemplaireId}")
    ReservationBean addEmprunt(@PathVariable("userId") int userId, @PathVariable("oeuvreId") int oeuvreId, @PathVariable("exemplaireId") int exemplaireId, @RequestBody EmpruntBean emprunt);

    @PatchMapping(value = "/emprunt/user/{userId}/oeuvre/{oeuvreId}/exemplaire/{exemplaireId}/{empruntId}")
    EmpruntBean updateEmprunt(@PathVariable("userId") int userId, @PathVariable("oeuvreId") int oeuvreId, @PathVariable("exemplaireId") int exemplaireId, @PathVariable("empruntId") int empruntId, @RequestBody EmpruntOutPutBean emprunt);


    // Auteur
    @PostMapping(path = "/auteur")
    Auteur addAuteur(@RequestBody Auteur auteur);

    @PostMapping(path = "/auteur/{auteurId}/livre/{livreId}")
    Auteur addAuteurLivre(@PathVariable("auteurId") int auteurId, @PathVariable("livreId") int livreId);

    @GetMapping(value = "/auteur/livre/{livreId}")
    List<Auteur> listeAuteursByLivre(@PathVariable("livreId") int livreId);

    @GetMapping(value = "/auteur/{auteurId}/livre")
    List<Oeuvre> listeLivreByAuteurs(@PathVariable("auteurId") int auteurId);

    @GetMapping(value = "/auteur")
    Iterable<Auteur> listeAuteurs();

    @GetMapping(value = "/auteur/{auteurId}")
    Auteur auteurById(@PathVariable("auteurId") int auteurId);

    @PutMapping(value = "/auteur/{auteurId}")
    Auteur updateAuteur(@RequestBody Auteur auteur, @PathVariable("auteurId") int auteurId);

    @DeleteMapping(value = "/auteur/{auteurId}")
    ResponseEntity<?> deleteAuteur(@PathVariable("auteurId") int auteurId);

}
